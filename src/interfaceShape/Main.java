package interfaceShape;

public class Main {
	 
    public static void main(String s[]) {
        Shape shapeRec = new Rectangle(10, 20);
        double rectangleArea = shapeRec.area();
        System.out.println("Rectangle area: " + rectangleArea);
 
        Shape shapeCir = new Circle(50);
        double circleArea = shapeCir.area();
        System.out.println("Circle area: " + circleArea);
        
        Shape shape1 = new Triangle(20, 20);
        double triangleArea = shape1.area();
        System.out.println("Triangle area: " + triangleArea);
    }
}
