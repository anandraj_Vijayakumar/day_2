package interfaceShape;

public class Circle implements Shape {
	 
    private int radius;
 
    Circle(int r) {	
        radius = r;
    }
    public double area() {
        return Math.PI * radius * radius ;
    }
}