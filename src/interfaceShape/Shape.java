package interfaceShape;

public interface Shape {
    double area();
}
