package array;

import java.util.Scanner;

public class TwoDimensionalArray {
	public static void main(String[] args){
		Scanner a = new Scanner(System.in);
		int [][] array = new int [2][2] ;
		int maxI=0 , maxJ=0 ;
		for(int i=0; i < array.length; i++) {
			for(int j=0; j < array.length; j++) {
				System.out.println("array ["+i+"] ["+j+"] = ");
				array [i][j] = a.nextInt();
			}
			
		}
		for(int i=0; i < array.length; i++) {
			for(int j=0; j < array.length; j++) {
				if(array[i][j] > array[maxI][maxJ]) {
					maxI = i;
					maxJ = j;
				}
				
			}
		}
		System.out.println("The  max of array is " +array[maxI][maxJ]);
		System.out.println("The position of max is array["+maxI+"]["+maxJ+"]");
	}

}
